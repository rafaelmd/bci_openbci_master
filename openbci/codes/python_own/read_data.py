#!/usr/bin/env python2.7.
import sys; sys.path.append('..') # help python find open_bci_v3.py relative to scripts folder
import open_bci_v3 as bci
import os
import matplotlib.pyplot as plt # for plot

# Which channel will be plotted
channel = 1;

def printData(sample):
	#os.system('clear')
	print "----------------"
	print("%f" %(sample.id))
	print sample.channel_data[channel]
	# print sample.aux_data
	# print "----------------"



if __name__ == '__main__':
	port = '/dev/ttyUSB0'
	baud = 115200
	board = bci.OpenBCIBoard(port=port)
	board.start_streaming(printData)
