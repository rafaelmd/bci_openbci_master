import collections
import matplotlib.pyplot as plt # for plot

# Plot the content of the circular buff as it is appended by the counter

d = collections.deque(maxlen=10)
t = collections.deque(maxlen=10)
# Plot figure setup
# plt.axis([0, 1000, 0, 1])
plt.ion() # turns iteractive mode on (dynamic update)
plt.show() # display the current figure

j = 0


for i in xrange(100):

	t.append(j) 
	d.append(i)
	plt.scatter(t, d)
	plt.draw()
	plt.hold(False) # hold is off
	j += 1
	
	print d
