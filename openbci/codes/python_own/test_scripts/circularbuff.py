import collections

# implements and print a circular buffer. The buffer is implemented as a deque (qeue) from
# collections lib.

d = collections.deque(maxlen=10)

for i in xrange(20):
	d.append(i)
	print d