#!/usr/bin/env python2.7.
import sys; sys.path.append('..') # help python find open_bci_v3.py relative to scripts folder
import open_bci_v3 as bci
import os
import matplotlib.pyplot as plt # for plot
import collections # circular buffer
import time # sleep functions
i = 0

def printData(sample):
	#os.system('clear')
	# print "----------------"
	# print("%f" %(sample.id))
	# print sample.channel_data[channel]
	global i
	i += 1

	dataBuff.append(sample.channel_data[channel])
	tempBuff.append(i)

	# print dataBuff
	# print tempBuff
	# tempBuff.append(sample.id)

	plt.plot(tempBuff, dataBuff)
	plt.draw()
    # time.sleep(0.05)

	# print sample.aux_data
	# print "----------------"

if __name__ == '__main__':
	# Circular buffer setup
	bufflen = 250

	dataBuff = collections.deque(maxlen = bufflen)
	tempBuff = collections.deque(maxlen = bufflen)

	# Which channel will be plotted
	channel = 1;

	# Plot figure setup
	# plt.axis([0, 1000, 0, 1])
	plt.ion()
	plt.show()
	plt.hold(False) # hold is off

	port = '/dev/ttyUSB0'
	baud = 115200
	board = bci.OpenBCIBoard(port=port, baud=baud)
	board.test_signal(3) # set all input channels to 0

	time.sleep(1) # need to include this to wait for test config setup

	board.start_streaming(printData)

