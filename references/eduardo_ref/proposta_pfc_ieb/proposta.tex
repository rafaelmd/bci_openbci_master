\documentclass[a4paper,11pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage{fancyhdr}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage[portuguese]{babel}
\usepackage{graphicx}
\usepackage{xcolor}

\title{Proposta de projeto \\ \textbf{Processamento de sinais de EEG para extração de informação motora}}

\author{Alexandre Trofino Neto, Cleison Silva, Eduardo Schmidt}
\begin{document}
\maketitle

\section{Apresentação do problema e motivação}
Contrariando estudos anteriores, alguns trabalhos mais recentes \cite{design_principles},\cite{decoding_grasp},\cite{comp_cursor} na área de interfaces cérebro-computador, têm mostrado que a atividade cerebral adquirida na forma de EEG no escalpo tem sim potencial para a extração de informação relacionada a intenções motoras detalhadas, como por exemplo as velocidades do "efetuador final" de um determinado membro, possivelmente a mão o usuário em um espaço 3D.

Isso significa que é possível extrair informação que tem potencial para ser utilizada no controle de dispositivos neuroprotéticos para a substituição de membros amputados, ou ainda para suporte a realização de movimentos específicos, no caso de alguma deficiência. Mais do que isso, alguns desses trabalhos conseguiram obter índices de correlação significativos entre os sinais de EEG medidos e o movimento imaginado em apenas alguns minutos de treinamento, em uma única sessão, o que representa uma menor necessidade de esforço por parte do usuário, melhorando sua experiência no uso do dispositivo em questão, e diminuindo sua frustração decorrente da dificuldade de adaptação ao mesmo, tão comum em sistemas comerciais.

Alguns dos trabalhos que obtiveram tais resultados utilizaram filtros para limitar a banda passante dos sinais adquiridos a frequências entre 1Hz e 30Hz, o que inclui baixas frequências, normalmente desprezadas em cenários de classificação de imaginação motora. Após a filtragem, os sinais foram utilizados para treinar um modelo baseado em um filtro de Wiener que relaciona o movimento imaginado (cuja obtenção será descrita a seguir) com o EEG medido no escalpo, através de uma função linear, conforme abaixo:
$$ 
 x[t] -x[t-1] = a_x + \overset{N}{\underset{n=1}{\sum}}\overset{L}{\underset{k=0}{\sum}} b_{nkx}S_n [t-k]
$$ 
$$
 y[t] -y[t-1] = a_y + \overset{N}{\underset{n=1}{\sum}}\overset{L}{\underset{k=0}{\sum}} b_{nky}S_n [t-k]
$$
$$
 z[t] -z[t-1] = a_z + \overset{N}{\underset{n=1}{\sum}}\overset{L}{\underset{k=0}{\sum}} b_{nkz}S_n [t-k]
$$

onde $x[t]$, $y[t]$ e $z[t]$ são as velocidades do movimento imaginado no espaço 3D, N é o número de sensores de EEG, L é o número de amostras anteriores consideradas, e $S_n [t-k]$ é a diferença temporal na tensão medida no sensor $n$ atrasada $k$ amostras.

Em alguns trabalhos estudados, a obtenção do movimento imaginado é feita indiretamente através da medição da velocidade de um cursor em uma tela, enquanto o usuário tenta imaginar que seu braço/dedo direito está seguindo o cursor.Neste caso, como o movimnto se dá em uma tela, portanto em um espaço 2D, tem-se que $z[t]=0$.
 
O treinamento do modelo consiste então em obter os parâmetros $a_x$, $a_y$, $b_{nkx}$ e $b_{nky}$ tais que a correlação entre os sinais de EEG medidos e o movimento imaginado é a máxima possível.

Conforme já mencionado, os resultados obtidos foram promissores. Para alguns sujeitos de teste, obteve-se correlação (Pearson) em torno de 0.7, o que corresponde a um valor maior até mesmo do que o obtido em trabalhos utilizando inclusive arrays de microeletrodos intracranianos.

\section{Objetivos e metas}
Este trabalho tem como objetivo a implementação de um sistema de extração de informação cinemática sinais de EEG obtidos durante a execução ou imaginação de uma tarefa motora contínua e de trajetória conhecida realizada em condições de laboratório. Tendo implementado o sistema, o mesmo deverá ser avaliado, e ajustes e melhorias serão feitas de forma a otimizar os resultados obtidos.

\section{Recursos e equipamentos}
Para o desenvolvimento desse projeto será necessário um espaço físico no Instituto de Engenharia Biomédica (IEB), equipado com computadores e sistema de aquisição de sinais de EEG.

\section{Riscos e dificuldades}
Dificuldades incluem a obtenção de dados de EEG adquiridos por terceiros em experimentos de execução de tarefas motoras que possam ser utilizados para a avaliação da corretude e desempenho dos algoritmos implementados, principalmente devido a questões éticas relacionadas ao compartilhamento de dados de ensaios envolvendo humanos. Além disso, no caso da realização de experimentos próprios, possivelmente enfrentaremos dificuldades relacionadas como por exemplo ruídos de medição, falta de experiência com os procedimentos de ensaio na área, etc.



\section{Atividades a serem desenvolvidas}
\begin{enumerate}
  \item Estudo sobre as técnicas utilizadas para extração de informação motora contínua de sinais de EEG;
  \item Obtenção de \textit{datasets} adquiridas por outros grupos de pesquisa;
  \item Implementação de algoritmo de extração de velocidades das juntas do braço ou da mão a partir de sinais de EEG utilizando um ou mais das técnicas pesquisadas;
  \item Avaliação dos resultados em testes realizados nas \textit{datasets} obtidas de terceiros;
  \item Implementação de um sistema online utilizando os equipamentos de aquisição solicitados e os algoritmos implementado;
  \item Avaliação dos resultados obtidos e proposta de possíveis melhorias;
  \item Em caso de sucesso, implementação de uma plataforma de testes, possivelmente com a utilização de um braço robótico, ou de uma interface gráfica para simular o movimento.
\end{enumerate}




\newpage    
\begin{thebibliography}{56}
\bibitem{design_principles}
  Contreras-Vidal J L and Bradberry T J;
  \emph{Design Principles for Noninvasive Brain-Machine Interfaces 2011}.
  33rd Annual International Conference of the IEEE EMBS
  
  
\bibitem{decoding_grasp}
  Harshavardhan A A and Contreras-Vidal J L;
  \emph{Decoding the evolving grasping gesture from electroencephalographic (EEG) activity 2013}.
  35th Annual International Conference of the IEEE EMBS
  
  
\bibitem{comp_cursor}
  Bradberry T J, Gentili R J, and Contreras-Vidal J L;
  \emph{Fast attainment of computer cursor control with noninvasively acquired brain signals 2011}.
  J. Neural Eng. 8 (2011)


  
  
\end{thebibliography}



\end{document}
