clear all

% A ordem dos algoritmos são os seguintes
% :
% CSP + LDA Ho = I
% CSP + LDA Ho = D
% MDM (Riemann) Ho = I
% MDM (Riemann) Ho = D
% MDM (Bhatta) Ho = I
% MDM (Bhatta) Ho = D

load('/home/rafael/Documents/aula_mestrado/estudo_dirigido/artigo_riemann/paraGraficoBarra.mat');

data_mean = results(:,10);

% exclude mean column and bhatta results
results(:,10) = [];
results(end,:) = [];
results(end,:) = [];

hold on
plot(data_mean,'kd');
ylabel('Acurracy (%)')

grid on;


boxplot(results', 'colors', 'k', 'boxstyle', 'outline');
% xticklabel_rotate([1:6],45,{'CSP + LDA Ho = I','CSP + LDA Ho = D','MDM (Rie) Ho = I','MDM (Rie) Ho = D','MDM (Bha) Ho = I','MDM (Bha) Ho = D'},'interpreter','none')

xticklabel_rotate([1:4],45,{'CSP + LDA Ho = I','CSP + LDA Ho = D','MDM (Rie) Ho = I','MDM (Rie) Ho = D'},'interpreter','none')

